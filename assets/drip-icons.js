import { createIconSetFromFontello } from 'react-native-vector-icons'
import fontelloConfig from './fonts/config.json'

export default createIconSetFromFontello(fontelloConfig)