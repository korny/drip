import { AppRegistry } from 'react-native'
import AppWrapper from './components/app-wrapper'

AppRegistry.registerComponent('home', () => AppWrapper)