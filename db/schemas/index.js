import schema0 from './0.js'
import schema1 from './1.js'
import schema2 from './2.js'

export default [schema0, schema1, schema2]